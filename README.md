# Home Bunker

This project contains network definitions and OS/Container installs for my home network servers and devices.

## Tools

Configure git for GitLab:

```sh
git config --global user.name "Vini"
git config --global user.email "3507182-baaase@users.noreply.gitlab.com"
```

# Credits

Project icon by [goodware std.](https://www.iconfinder.com/goodware), licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/), available at [iconfinder.com](https://www.iconfinder.com/icons/2760615/army_bomb_grenade_military_navy_tank_weapon_icon).
