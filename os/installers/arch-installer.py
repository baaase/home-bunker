import os
import sys
import json
import re

if len(sys.argv) != 2:
    raise Exception(f"""Wrong number of arguments.\nUsage: {sys.argv[0]} <config-file>""")

with open(os.path.dirname(sys.argv[0]) + "./arch-installer-templates.json") as contents:
    templates = json.load(contents)

with open(sys.argv[1]) as contents:
    configuration = json.load(contents)

deployment = {
    "host": {
        "fullname": configuration["general"]["host"],
        "name": re.search(r"[^.]*", configuration["general"]["host"]).group(0),
        "locale-gen": configuration["general"]["locale-gen"],
    },
    "profiles": {p: templates["profiles"][p] for p in configuration["profile-list"] if p in templates["profiles"]},
    "installer": {
        "install-dir": templates["installer"]["install-dir"]
    },
    "actions": ["cd /"],
}

### Validations
# Domain
if "." in configuration["general"]["host"]:
    deployment["host"]["domain"] = re.search(r"\..*", configuration["general"]["host"]).group(0)
if configuration["general"].get("localtime"):
    deployment["host"]["localtime"] = configuration["general"]["localtime"]
if templates["installer"].get("keys-dir"):
    deployment["installer"]["keys-dir"] = templates["installer"].get("keys-dir")
    deployment["installer"]["keys-prefix"] = templates["installer"].get("keys-prefix") or ""
    deployment["installer"]["keys-suffix"] = templates["installer"].get("keys-suffix") or ""
# Keyboard layout
if configuration["general"].get("keyboard-layout"):
    deployment["keyboard-layout"] = templates["keyboard-layouts"][configuration["general"].get("keyboard-layout")]

### Preparations over variables
deployment["packages"] = [p for d in deployment["profiles"].values() if d.get("package-list") for p in d.get("package-list")]
deployment["services"] = [s for d in deployment["profiles"].values() if d.get("service-list") for s in d.get("service-list")]
deployment["block-devices"] = {d: b for d, b in configuration["storage"].get("block-devices").items()}
deployment["lvm-devices"] = {d: b for d, b in configuration["storage"].get("lvm-devices").items()}
deployment["devices"] = {}
if "encryption-keys-device" in configuration["general"] and "keys-dir" in deployment["installer"]:
    deployment["encryption-keys-device"] = configuration["general"]["encryption-keys-device"]

### Set terminal keyboard
x = exec("deployment['keyboard-layout']['loadkeys']")
### TODO keyboard layouts
# print("//" + str(x) + "//")

# if deployment.get("keyboard-layout"):
#     deployment["actions"].append(" ".join(["loadkeys", f"""\"{deployment["keyboard-layout"]["loadkeys"]}\""""]))

wants_prefix_default = True if configuration["storage"].get("prefix-with-hostname") else False
### Block devices: partitioning
for device, block in deployment["block-devices"].items():
    will_partition = block.get('repartition-to') and block['repartition-to'] == "gpt"
    wants_prefix_block = block.get("prefix-with-hostname")
    if wants_prefix_block is None:
        wants_prefix_block = wants_prefix_default
    if will_partition:
        fdisk_cmd = ["g"] # new gpt partition

    deployment["devices"][device] = { "device": device, "originallabel": device }

    partition_no = 1
    for partlabel, partition in block.get("partition-list").items():
        will_format = will_partition or partition.get("format")
        will_encrypt = partition.get("encrypt") and will_format # Do not regenerate crypts if not partitioning nor formatting
        will_mount = partition.get("mountpoint")
        will_prefix = partition.get("prefix-with-hostname")
        if will_prefix is None:
            will_prefix = wants_prefix_block

        _partlabel = (f"""{deployment["host"]["name"]}-""" if will_prefix else "") + partlabel
        _device_partlabel = "/dev/disk/by-partlabel/" + _partlabel
        _device_mapped = "/dev/mapper/" + _partlabel + "-x" if will_encrypt else _device_partlabel
        _keyfile = f"""{deployment["installer"]["keys-dir"]}/{deployment["installer"]["keys-prefix"]}{_partlabel}{deployment["installer"]["keys-suffix"]}"""

        deployment["devices"][_device_mapped] = { "device": device + str(partition_no), "partlabel": _partlabel, "format": partition["type"], "originallabel": partlabel, "dependencies": [device] }
        _cmds = []

        if will_partition:
            partition_create = ["n", str(partition_no), "", "" if partition["size"] == "*" else f"""+{partition["size"]}"""]
            partition_type = ["t", templates["filesystems"]["fdisk"][partition["type"]]]
            partition_name = ["x", "n", _partlabel, "r"]
            if partition_no > 1:
                partition_type.insert(1, str(partition_no))
                partition_name.insert(2, str(partition_no))
            fdisk_cmd.extend(partition_create) # create partition
            fdisk_cmd.extend(partition_type) # set partition type
            fdisk_cmd.extend(partition_name) # set partition name

        if will_encrypt:
            _cmds.append(" ".join(["dd", "bs=512", "count=8000", "if=\"/dev/random\"", f"""of=\"{_keyfile}\"""", "iflag=fullblock"]))
            _cmds.append(" ".join(["cryptsetup", "--batch-mode", "luksFormat", f"""\"{_device_partlabel}\"""", f"""\"{_keyfile}\""""]))
            _cmds.append(" ".join(["cryptsetup", "--batch-mode", "open", f"""\"{_device_partlabel}\"""", f"""\"{_partlabel}-x\"""", "--key-file", f"""\"{_keyfile}\""""]))
            deployment["devices"][_device_mapped]["dependencies"] = deployment["devices"][_device_mapped].get("dependencies") or []
            deployment["devices"][_device_mapped]["dependencies"].append(deployment["encryption-keys-device"])
            deployment["devices"][_device_mapped]["encrypt"] = True

        if will_format:
            cmd_list = templates["filesystems"]["mkfs"][partition["type"]].copy()
            cmd_list.append(f"""\"{_device_mapped}\"""")
            _cmds.append(" ".join(cmd_list))

        if will_mount:
            deployment["devices"][_device_mapped]["mountpoint"] = will_mount
            if partition.get("encrypt"):
                deployment["devices"][_device_mapped]["keyfile"] = _keyfile

        if _cmds:
            deployment["devices"][_device_mapped]["cmds"] = _cmds

        partition_no += 1
    if will_partition:
        fdisk_cmd.extend(["w"]) #write to disk
        deployment["devices"][device]["cmds"] = [" ".join(["fdisk", f"""\"{device}\"""", "<<", "EOF\n" + "\n".join(fdisk_cmd) + "\nEOF\n"])]

### LVM devices: partitioning
for vg_label, vg in deployment["lvm-devices"].items():
    will_partition = True
    will_prefix_vg = vg.get("prefix-with-hostname")
    if will_prefix_vg is None:
        will_prefix_vg = wants_prefix_default
    _vg_label = (f"""{deployment["host"]["name"]}-""" if will_prefix_vg else "") + vg_label
    _device_mapped_vg = f"""/dev/mapper/{"--".join(_vg_label.split("-"))}"""

    deployment["devices"][_device_mapped_vg] = { "originallabel": vg_label }
    deployment["devices"][_device_mapped_vg]["dependencies"] = []
    for l, cfg in deployment["devices"].items():
        if cfg["originallabel"] in vg["devices"]:
            deployment["devices"][_device_mapped_vg]["dependencies"].append(l)
    if len(deployment["devices"][_device_mapped_vg]["dependencies"]) != len(vg["devices"]):
        raise "Number of devices specified in LVM not matching number devices found for LVM."
    _cmds_vg = [ "vgcreate", f"""\"{_vg_label}\""""]
    _cmds_vg.extend([f"""\"{d}\"""" for d in deployment["devices"][_device_mapped_vg]["dependencies"]])
    deployment["devices"][_device_mapped_vg]["cmds"] = [ " ".join(_cmds_vg)]

    for lv_label, lv in vg.get("logical-volumes").items():
        will_format = will_partition or lv.get("format")
        will_mount = lv.get("mountpoint")

        _lv_label = (f"""{deployment["host"]["name"]}-""" if lv.get("prefix-with-hostname") else "") + lv_label
        _device_mapped = f"""{_device_mapped_vg}-{"--".join(_lv_label.split("-"))}"""

        deployment["devices"][_device_mapped] = { "format": lv["type"], "originallabel": lv_label, "dependencies": [ _device_mapped_vg ] }
        _cmds = []

        if will_format:
            _cmds.append(" ".join(["lvcreate", "--extents" if lv["size"] == "*" else"--size", "100%FREE" if lv["size"] == "*" else f"""{lv["size"]}""", f"""\"{_vg_label}\"""", "-n", f"""\"{_lv_label}\""""]))
            cmd_list = templates["filesystems"]["mkfs"][lv["type"]].copy()
            cmd_list.append(f"""\"{_device_mapped}\"""")
            _cmds.append(" ".join(cmd_list))

        if will_mount:
            deployment["devices"][_device_mapped]["mountpoint"] = will_mount
            if partition.get("encrypt"):
                deployment["devices"][_device_mapped]["keyfile"] = _keyfile

        if _cmds:
            deployment["devices"][_device_mapped]["cmds"] = _cmds

final_list = list([d for d in deployment["devices"]])
devices = list(deployment["devices"].items())
index = len(final_list) - 1
while index > 0:
    _index = index
    for d, cfg in devices:
        if "dependencies" in cfg and final_list[index] in cfg["dependencies"]:
            _index = min(_index, final_list.index(d))
    if (_index < index):
        item = final_list.pop(index)
        final_list.insert(_index, item)
    else:
        index -= 1
deployment["device-order"] = final_list

enc_device = deployment["encryption-keys-device"] if "encryption-keys-device" in deployment else None
# This will build the actions based on device-order, and also propagate the meta
for d in deployment["device-order"]:
    if "cmds" in deployment["devices"][d]:
        deployment["actions"].extend(deployment["devices"][d]["cmds"])
    if d == enc_device:
        deployment["actions"].append(" ".join(["mount", "--mkdir", d, f"""\"{deployment["installer"]["keys-dir"]}\""""]))
    # Initialize some variables for every device
    deployment["devices"][d]["parent-devices"] = set()
    deployment["devices"][d]["parent-partlabels"] = set()
if enc_device:
    deployment["actions"].append(" ".join(["umount", "-R", f"""\"{deployment["installer"]["keys-dir"]}\""""]))

# Propagate device and partlabel
for d in deployment["device-order"]:
    if "dependencies" in deployment["devices"][d]:
        for dep in deployment["devices"][d]["dependencies"]:
            if "device" in deployment["devices"][dep]:
                deployment["devices"][d]["parent-devices"].add(deployment["devices"][dep]["device"])
            if "partlabel" in deployment["devices"][dep]:
                deployment["devices"][d]["parent-partlabels"].add(deployment["devices"][dep]["partlabel"])
            deployment["devices"][d]["parent-devices"].union(deployment["devices"][dep]["parent-devices"])
            deployment["devices"][d]["parent-partlabels"].union(deployment["devices"][dep]["parent-partlabels"])

for d in deployment["device-order"]:
    deployment["devices"][d]["parent-devices"] = list(deployment["devices"][d]["parent-devices"])
    deployment["devices"][d]["parent-partlabels"] = list(deployment["devices"][d]["parent-partlabels"])

### Mount devices in final location (except)
deployment["mountpoints"] = {}
for d in deployment["devices"]:
    if "mountpoint" in deployment["devices"][d]:
        if deployment["devices"][d]["mountpoint"] in deployment["mountpoints"]:
            raise "Mountpoint specified multiple times"
        deployment["mountpoints"][deployment["devices"][d]["mountpoint"]] = d
deployment["mount-order"] = sorted(list([m for m in deployment["mountpoints"]]))

for m in deployment["mount-order"]:
    deployment["actions"].append(" ".join(["mount", "--mkdir", f"""\"{deployment["mountpoints"][m]}\"""", f"""\"{deployment["installer"]["install-dir"]}{m}\""""]))

### Install arch
pacstrap_cmd = ["pacstrap", "-K", f"""\"{deployment["installer"]["install-dir"]}\""""]
pacstrap_cmd.extend(deployment["packages"])
deployment["actions"].append(" ".join(pacstrap_cmd))

### Generate fstab
deployment["actions"].append(" ".join(["genfstab", "-t", "PARTLABEL", "-p", f"""\"{deployment["installer"]["install-dir"]}\"""", ">>", f"""\"{deployment["installer"]["install-dir"]}/etc/fstab\""""]))
### TODO: noauto, nofail

### chroot
chroot_cmd = ["arch-chroot", f"""\"{deployment["installer"]["install-dir"]}\""""]

### hostname
_cmd = chroot_cmd.copy()
_cmd.extend(["echo", f"""\"{deployment["host"]["fullname"]}\"""", ">", "/etc/hostname"])
deployment["actions"].append(" ".join(_cmd))

### Set localtime
if "localtime" in deployment["host"]:
    _cmd = chroot_cmd.copy()
    _cmd.extend(["rm", "-f", "/etc/localtime"])
    deployment["actions"].append(" ".join(_cmd))
    _cmd = chroot_cmd.copy()
    _cmd.extend(["ln", "-s", f"""\"{deployment["host"]["localtime"]}\"""", f"""{deployment["installer"]["install-dir"]}/etc/localtime"""])
    deployment["actions"].append(" ".join(_cmd))
    _cmd = chroot_cmd.copy()
    _cmd.extend(["hwclock", "--systohc"])
    deployment["actions"].append(" ".join(_cmd))

### locale
for locale in deployment["host"]["locale-gen"]:
    _cmd = chroot_cmd.copy()
    _cmd.extend(["echo", f"""\"{locale}\"""", ">>", f"""{deployment["installer"]["install-dir"]}/etc/locale.gen"""])
    deployment["actions"].append(" ".join(_cmd))
    _cmd = chroot_cmd.copy()
    _cmd.extend(["locale-gen"])
    deployment["actions"].append(" ".join(_cmd))
    _cmd = chroot_cmd.copy()
    _cmd.extend(["locale", ">", f"""{deployment["installer"]["install-dir"]}/etc/locale.conf"""])
    deployment["actions"].append(" ".join(_cmd))

### keyboard-layout
if "keyboard-layout" in deployment:
    _cmd = chroot_cmd.copy()
    _cmd.extend(["echo", f"""\"KEYMAP={deployment["keyboard-layout"]["loadkeys"]}\"""", ">>", f"""\"{deployment["installer"]["install-dir"]}/etc/vconsole.conf\""""])
    deployment["actions"].append(" ".join(_cmd))

### bootloader
_cmd = chroot_cmd.copy()
_cmd.extend(["sed", "--in-place", "-E", f"""\'/^HOOKS=/ s/ *encrypt */ /g; /^HOOKS=/ s/ *lvm2 */ /g; /^HOOKS=/ s/[ ]+/ /g; /^HOOKS=/ s/filesystems/encrypt lvm2 filesystems/g\'""", f"""\"/etc/mkinitcpio.conf\""""])
deployment["actions"].append(" ".join(_cmd))
_cmd = chroot_cmd.copy()
_cmd.extend(["mkinitcpio", "-P"])
deployment["actions"].append(" ".join(_cmd))
_cmd = chroot_cmd.copy()
_cmd.extend(["bootctl", "--path=/boot/", "install"])
deployment["actions"].append(" ".join(_cmd))
_cmd = chroot_cmd.copy()
deployment["actions"].append(" ".join(["echo", ">", f"""\"{deployment["installer"]["install-dir"]}/boot/loader/loader.conf\"""", "default arch"]))
deployment["actions"].append(" ".join(["echo", ">>", f"""\"{deployment["installer"]["install-dir"]}/boot/loader/loader.conf\"""", "timeout 0"]))
deployment["actions"].append(" ".join(["echo", ">>", f"""\"{deployment["installer"]["install-dir"]}/boot/loader/loader.conf\"""", "editor 0"]))
deployment["actions"].append(" ".join(["echo", ">", f"""\"{deployment["installer"]["install-dir"]}/boot/loader/entries/arch.conf\"""", "title Arch Linux"]))
deployment["actions"].append(" ".join(["echo", ">>", f"""\"{deployment["installer"]["install-dir"]}/boot/loader/entries/arch.conf\"""", "linux /vmlinuz-linux"]))
deployment["actions"].append(" ".join(["echo", ">>", f"""\"{deployment["installer"]["install-dir"]}/boot/loader/entries/arch.conf\"""", "initrd /initramfs-linux.img"]))
# deployment["actions"].append(" ".join(["echo", ">>", f"""\"{deployment["installer"]["install-dir"]}/boot/loader/entries/arch.conf\"""", f"""options cryptdevice=PARTLABEL={deployment["devices"][deployment["mountpoints"]["/"]]["parent-partlabels"][0]}:{deployment["devices"][deployment["mountpoints"]["/"]]["parent-partlabels"][0]}-x cryptkey={deployment["encryption-keys-device"]}:{deployment["devices"][deployment["encryption-keys-device"]]["format"]}:/{deployment["installer"]["keys-prefix"]}{deployment["devices"][deployment["mountpoints"]["/"]]["parent-partlabels"][0]}{deployment["installer"]["keys-suffix"]} root={deployment["mountpoints"]["/"]} quiet rw"""]))

### Crypttab
# for d in deployment["devices"]:
#     if d not in deployment["devices"][deployment["mountpoints"]["/"]]["parent-devices"] and deployment["devices"][d].get("encrypt"):
#         print(d)
#         print(deployment["devices"][d])
#         deployment["actions"].append(" ".join(["echo", ">>", f"""\"{deployment["installer"]["install-dir"]}/etc/crypttab\"""", f"""{deployment["devices"][d]["partlabel"]}-x  {deployment["devices"][d]["device"]}  /{deployment["installer"]["keys-prefix"]}{deployment["devices"][deployment["mountpoints"]["/"]]["parent-partlabels"][0]}{deployment["installer"]["keys-suffix"]}"""]))

### TODO crypttab
### TODO passwd
### TODO file-list
### TODO switch echo for printf

# print(json.dumps(deployment, indent=4))
# print(json.dumps(deployment["devices"], indent=4))
# print(json.dumps(deployment["device-order"], indent=4))
# print(json.dumps(deployment["actions"], indent=4))
for action in deployment["actions"]:
    print(action)
