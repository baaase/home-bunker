# Arch Linux workstation installer

This document describes the architecture in which the Arch Linux workstation will be installed, as well as the install procedures.

## Architecture of the generated install

1. Mandatory in UEFI boot mode
1. Installs in single block device, partitioned as:
   1. EFI partition, 255MB, formatted as FAT32
   1. LVM partition, remainder of the block device, partitioned as:
      1. Root partition, using all the space available on LVM, formatted as ext4
1. By default, the admin user is member of the wheel group
1. By default, timesyncd, dhcpcd and sshd are active

## Using this installer

1. Start arch-linux from the installation media
1. Install git in the RAM disk, using:
   - `pacman -Sy --noconfirm git`
1. Clone this project from gitlab, using:
   - `git clone https://gitlab.com/baaase/home-bunker.git`
1. Access the cloned directory:
   - `cd home-bunker/os-installers`
1. Copy _arch-workstation.cfg_ to a new file (in this example, _my-ws.cfg_):
   - `cp arch-workstation.cfg my-ws.cfg`
1. Update the new configurations as needed:
   - `vim my-ws.cfg`
1. Run the installer, instructing it to use the new configuration file (**Careful: the installer will NOT ASK CONFIRMATIONS. You may lose all your data upon misuse.**):
   - `./arch-workstation.sh my-ws.cfg`
