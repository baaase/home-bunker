#!/bin/bash

# ----- Error Checking -----
if [ "$#" -ne "1" ]; then
  echo "usage: \"${0}\" <config file>"
  exit 1
elif [ ! -f "${1}" ]; then
  echo "Invalid file: \"${1}\""
  exit 2
elif [ $(id -u) -ne 0 ]; then
  echo "This script must be run as root" 
  exit 3
fi

source "${1}" || exit 255

if [ -z ${install_admin_user+x} ]; then
  echo -e "Missing required parameter: install_admin_user"
  exit 4
elif [ -z ${install_boot_device+x} ]; then
  echo -e "Missing required parameter: install_boot_device"
  exit 4
elif [ -z ${install_host+x} ]; then
  echo -e "Missing required parameter: install_host"
  exit 4
elif [[ "${install_host// /}" == "" ]] || [[ "${install_host//[A-Za-z0-9\_\-]/}" != "" ]]; then
  echo -e "Invalid host name: \"${install_host}\""
  exit 5
fi
# [ -z ${install_domain+x} ] && install_domain=""
[ -z ${install_base_pkg_list+x} ] && install_base_pkg_list="base openssh"
[ -z ${install_base_svc_list+x} ] && install_base_svc_list="systemd-timesyncd.service dhcpcd.service sshd.service"
[ -z ${install_base_admin_group_list+x} ] && install_base_admin_group_list="wheel"

if [ ! -b "${install_boot_device}" ]; then
  echo "install_boot_device is not set to a valid device"
  exit 6
fi

if [[ "${install_domain}" != "" ]]; then
  install_domain_suffix=".${install_domain}"
else
  install_domain_suffix=""
fi


# ----- Start install -----

##### Load keyboard layout
if [ ! -z ${install_keymap+x} ]; then
  loadkeys "${install_keymap}" || exit 255
fi

##### Partition and mount the root disk
echo -e "Partioning boot disk"
echo -e "g\nn\n\n\n+255M\nt\n1\nx\nn\n${install_host}-efi\nr\nn\n\n\n\nt\n\n31\nx\nn\n\n${install_host}-vg-root\nr\nw\n" | fdisk ${install_boot_device} -w always

echo -e "Setting up LVM"
sleep 1 # Workaround for sometimes LVM fails because the partlabel link is not yet created
pvcreate /dev/disk/by-partlabel/${install_host}-vg-root || exit 255
vgcreate ${install_host}-vg-root /dev/disk/by-partlabel/${install_host}-vg-root || exit 255
lvcreate -l100%FREE ${install_host}-vg-root -n lv-root || exit 255

echo -e "Formating disks"
mkfs.fat -F32 /dev/disk/by-partlabel/${install_host}-efi || exit 255
mkfs.ext4 -F /dev/mapper/${install_host//-/--}--vg--root-lv--root || exit 255

echo -e "Mounting disks"
mount /dev/mapper/${install_host//-/--}--vg--root-lv--root /mnt || exit 255
mkdir -p /mnt/boot || exit 255
mount /dev/disk/by-partlabel/${install_host}-efi /mnt/boot || exit 255

echo -e "Preparing to install"
install_pkg_list="${install_base_pkg_list}"
install_svc_list="${install_base_svc_list}"
install_admin_group_list="${install_base_admin_group_list}"

if [ "${install_git^^}" == "Y" ]; then
  echo -e "Adding git"
  install_pkg_list="${install_pkg_list} git"
fi

if [ "${install_docker^^}" == "Y" ]; then
  echo -e "Adding docker"
  install_pkg_list="${install_pkg_list} docker"
  install_svc_list="${install_svc_list} docker.service"
  install_admin_group_list="${install_admin_group_list} docker"
fi

if [ "${install_vbox_guest^^}" == "Y" ]; then
  echo -e "Adding VirtualBox guest additions"
  install_pkg_list="${install_pkg_list} virtualbox-guest-utils virtualbox-guest-modules-arch"
  install_svc_list="${install_svc_list} vboxservice.service"
elif [ "${install_vbox_guest^^}" == "NOX" ]; then
  echo -e "Addng VirtualBox guest additions (NO X)"
  install_pkg_list="${install_pkg_list} virtualbox-guest-utils-nox virtualbox-guest-modules-arch"
  install_svc_list="${install_svc_list} vboxservice.service"
fi

if [ "${install_xfce^^}" == "Y" ]; then
  echo -e "Adding XFCE"
  install_pkg_list="${install_pkg_list} xfce4 lxdm xorg-server chromium ttf-dejavu"
  install_svc_list="${install_svc_list} lxdm.service"
fi

echo -e "Install Arch linux"
pacstrap /mnt ${install_pkg_list} || exit 255
echo -e "/dev/mapper/${install_host}--vg--root-lv--root\t/\text4\trw,relatime\t0\t1" > /mnt/etc/fstab
echo -e "/dev/disk/by-partlabel/${install_host}-efi\t/boot\tvfat\trw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro\t0\t2" >> /mnt/etc/fstab

echo -e "Updating configurations"
arch-chroot /mnt systemctl enable ${install_svc_list} || exit 255
arch-chroot /mnt ln -sTf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime || exit 255
arch-chroot /mnt hwclock --systohc || exit 255
echo -e "en_US.UTF-8 UTF-8" >> /mnt/etc/locale.gen
arch-chroot /mnt locale-gen || exit 255
echo -e "LANG=en_US.UTF-8" >> /mnt/etc/locale.conf
[ ! -z ${install_keymap+x} ] && echo -e "KEYMAP=${install_keymap}" >> /mnt/etc/vconsole.conf
echo -e "${install_host}${install_domain_suffix}" > /mnt/etc/hostname
echo -e "127.0.0.1\tlocalhost" >> /mnt/etc/hosts
echo -e "127.0.0.1\t${install_host}${install_domain_suffix}\t${install_host}" >> /mnt/etc/hosts

echo -e "Setting up boot configurations"
sed -r -E -i 's/(^HOOKS=.*[^[:blank:]])[[:blank:]]*(lvm2|keyboard|keymap)[[:blank:]]*(.*)/\1 \3/g' /mnt/etc/mkinitcpio.conf || exit 255
sed -r -E -i 's/(^HOOKS=.*)block(.*)/\1block lvm2\2/g' /mnt/etc/mkinitcpio.conf || exit 255
sed -r -E -i 's/(^HOOKS=.*)autodetect(.*)/\1autodetect keyboard keymap\2/g' /mnt/etc/mkinitcpio.conf || exit 255
arch-chroot /mnt mkinitcpio -p linux || exit 255
arch-chroot /mnt bootctl --path=/boot install || exit 255
echo -e "default ${install_host}" > /mnt/boot/loader/loader.conf
echo -e "timeout 0" >> /mnt/boot/loader/loader.conf
echo -e "title \"Boot ${install_host}\"" > /mnt/boot/loader/entries/${install_host}.conf
echo -e "linux /vmlinuz-linux" >> /mnt/boot/loader/entries/${install_host}.conf
echo -e "initrd /initramfs-linux.img" >> /mnt/boot/loader/entries/${install_host}.conf
echo -e "options root=/dev/mapper/${install_host//-/--}--vg--root-lv--root quiet rw" >> /mnt/boot/loader/entries/${install_host}.conf

echo -e "Saving the install configuration into /etc/home-bunker"
mkdir -p /mnt/etc/home-bunker || exit 255
cp "${1}" /mnt/etc/home-bunker/ || exit 255

echo -e "Setup \"root\" password"
arch-chroot /mnt passwd || exit 255

echo -e "Setup \"${install_admin_user}\" password"
arch-chroot /mnt useradd -m -g users -G ${install_admin_group_list// /,} -s /bin/bash "${install_admin_user}" || exit 255
arch-chroot /mnt passwd "${install_admin_user}" || exit 255

if [ "${install_xfce^^}" == "Y" ]; then
  echo -e "Configuring X keyboard map"
  if [ ! -z ${install_keymap_X+x} ]; then
    arch-chroot /mnt localectl --no-convert set-x11-keymap "${install_keymap_X}" || exit 255
  fi
fi

echo -e "Reboot your system for changes to take effect!"
# reboot