# Open a Shell for required apps
$Shell = New-Object -ComObject Shell.Application

# Remove useless content
$BlockListed_AppxPackage = New-Object -TypeName System.Collections.ArrayList
$BlockListed_AppxPackage.AddRange(@(
    "Microsoft.549981C3F5F10",
    "Microsoft.BingWeather",
    "Microsoft.GetHelp",
    "Microsoft.Getstarted",
    "Microsoft.Microsoft3DViewer",
    "Microsoft.MicrosoftOfficeHub",
    "Microsoft.MicrosoftSolitaireCollection",
    "Microsoft.MicrosoftStickyNotes",
    "Microsoft.MixedReality.Portal",
    "Microsoft.MSPaint",
    "Microsoft.Office.OneNote",
    "Microsoft.People",
    "Microsoft.SkypeApp",
    "Microsoft.Wallet",
    "Microsoft.WindowsAlarms",
#    "Microsoft.WindowsCamera",
    "microsoft.windowscommunicationsapps",
    "Microsoft.WindowsFeedbackHub",
    "Microsoft.WindowsMaps",
    "Microsoft.Xbox.TCUI",
    "Microsoft.XboxApp",
#    "Microsoft.XboxGameOverlay",
#    "Microsoft.XboxGamingOverlay",
#    "Microsoft.XboxIdentityProvider",
    "Microsoft.XboxSpeechToTextOverlay",
    "Microsoft.YourPhone",
    "Microsoft.ZuneMusic",
    "Microsoft.ZuneVideo",
    "Disney.37853FC22B2CE",
    "SpotifyAB.SpotifyMusic"
))

$Provisioned_AppxPackage = Get-AppxPackage | Select-Object -ExpandProperty Name
foreach ($AppxPackage in $Provisioned_AppxPackage) {
    if ($AppxPackage -in $BlockListed_AppxPackage) {
        Get-AppxPackage -Name $AppxPackage | Remove-AppxPackage
    }
}

$BlockListed_WindowsCapabilities = "App.StepsRecorder|App.Support.QuickAssist|Browser.InternetExplorer|MathRecognizer|Print.Fax.Scan|Microsoft.Windows.Notepad|Microsoft.Windows.WordPad"
$Installed_WindowsCapabilities = Get-WindowsCapability -Online -LimitAccess -ErrorAction Stop | Where-Object { $_.Name -match $BlockListed_WindowsCapabilities -and $_.State -like "Installed" } | Select-Object -ExpandProperty Name
foreach ($WindowsCapability in $Installed_WindowsCapabilities) {
    try {
        Get-WindowsCapability -Online -LimitAccess -ErrorAction Stop | Where-Object { $_.Name -like $WindowsCapability } | Remove-WindowsCapability -Online -ErrorAction Stop | Out-Null
    }
    catch [System.Exception] {
        Write-Error -Message "Removing Windows Capability failed: $($_.Exception.Message)"
    }
}

$BlockListed_WindowsOptionalFeatures = "Printing-XPSServices-Features|SMB1Protocol"
$Installed_WindowsOptionalFeatures = Get-WindowsOptionalFeature -Online | Where-Object { $_.FeatureName -match $BlockListed_WindowsOptionalFeatures -and $_.State -like "Enabled" } | Select-Object -ExpandProperty FeatureName
foreach ($WindowsOptionalFeature in $Installed_WindowsOptionalFeatures) {
    try {
        Get-WindowsOptionalFeature -Online -FeatureName $WindowsOptionalFeature | Disable-WindowsOptionalFeature -Online -NoRestart
    }
    catch [System.Exception] {
        Write-Error -Message "Removing Windows Optional Feature failed: $($_.Exception.Message)"
    }
}

# $Wanted_WindowsOptionalFeatures = "Linux"
# $Disabled_WindowsOptionalFeatures = Get-WindowsOptionalFeature -Online | Where-Object { $_.FeatureName -match $Wanted_WindowsOptionalFeatures -and $_.State -like "Disabled" } | Select-Object -ExpandProperty FeatureName
# foreach ($WindowsOptionalFeature in $Disabled_WindowsOptionalFeatures) {
#     try {
#         Get-WindowsOptionalFeature -Online -FeatureName $WindowsOptionalFeature | Enable-WindowsOptionalFeature -Online -NoRestart
#     }
#     catch [System.Exception] {
#         Write-Error -Message "Adding Windows Optional Feature failed: $($_.Exception.Message)"
#     }
# }

$Blocklisted_Files = New-Object -TypeName System.Collections.ArrayList
$Blocklisted_Files.AddRange(@(
    "$env:PUBLIC\Desktop\Google Chrome.lnk",
    "$env:PUBLIC\Desktop\Microsoft Edge.lnk",
    "$env:USERPROFILE\Desktop\GitHub Desktop.lnk",
    "$env:USERPROFILE\Desktop\WinSCP.lnk"
))
foreach ($File in $Blocklisted_Files) {
    try {
        Remove-Item $File -ErrorAction Ignore
    }
    catch [System.Exception] {
        Write-Error -Message "Removing File failed: $($_.Exception.Message)"
    }
}

# Configure Quick Access Pins in File Explorer
$Blocklisted_QuickAccessPins = New-Object -TypeName System.Collections.ArrayList
$Blocklisted_QuickAccessPins.AddRange(@(
    "$env:USERPROFILE\Pictures"
))
$Unwanted_QuickAccessPins = $Shell.Namespace("shell:::{679F85CB-0220-4080-B29B-5540CC05AAB6}").Items() | Where-Object { $_.Path -in $Blocklisted_QuickAccessPins }
foreach ($QuickAccessPin in $Unwanted_QuickAccessPins) {
    $QuickAccessPin.InvokeVerb("unpinfromhome")
}

$Wanted_QuickAccessPins = New-Object -TypeName System.Collections.ArrayList
$Wanted_QuickAccessPins.AddRange(@(
    "$env:USERPROFILE"
))
foreach ($QuickAccessPin in $Wanted_QuickAccessPins) {
    $Shell.Namespace($QuickAccessPin).Self.InvokeVerb("pintohome")
}

# Configure power options
powercfg /change monitor-timeout-dc 5
powercfg /change monitor-timeout-ac 10
powercfg /change standby-timeout-dc 10
powercfg /change standby-timeout-ac 15
# Configure actions
## Closing the lid: do nothing (change the 0 to 1 to set to sleep)
powercfg -setdcvalueindex SCHEME_CURRENT 4f971e89-eebd-4455-a8de-9e59040e7347 5ca83367-6e45-459f-a27b-476b1d01c936 0
powercfg -setacvalueindex SCHEME_CURRENT 4f971e89-eebd-4455-a8de-9e59040e7347 5ca83367-6e45-459f-a27b-476b1d01c936 0
## Other actions
powercfg -setdcvalueindex SCHEME_CURRENT 4f971e89-eebd-4455-a8de-9e59040e7347 96996bc0-ad50-47ec-923b-6f41874dd9eb 1
powercfg -setacvalueindex SCHEME_CURRENT 4f971e89-eebd-4455-a8de-9e59040e7347 96996bc0-ad50-47ec-923b-6f41874dd9eb 1
powercfg -setdcvalueindex SCHEME_CURRENT 4f971e89-eebd-4455-a8de-9e59040e7347 7648efa3-dd9c-4e3e-b566-50f929386280 3
powercfg -setacvalueindex SCHEME_CURRENT 4f971e89-eebd-4455-a8de-9e59040e7347 7648efa3-dd9c-4e3e-b566-50f929386280 3
# Configure screen brightness to 0
powercfg -setdcvalueindex SCHEME_CURRENT 7516b95f-f776-4464-8c53-06167f40cc99 aded5e82-b909-4619-9949-f5d71dac0bcb 0
powercfg -setacvalueindex SCHEME_CURRENT 7516b95f-f776-4464-8c53-06167f40cc99 aded5e82-b909-4619-9949-f5d71dac0bcb 0

# Remove OneDrive
taskkill /f /im OneDrive.exe
& "$env:SystemRoot\SysWOW64\OneDriveSetup.exe" /uninstall

# Activate Windows Defender
Set-MpPreference -DisableRealtimeMonitoring $false

# Create Desktop Directories
New-Item -Path "$env:USERPROFILE\Desktop\Files\Media\Lockscreen" -Type Directory -ErrorAction Ignore
New-Item -Path "$env:USERPROFILE\Desktop\Files\Media\Wallpapers" -Type Directory -ErrorAction Ignore

# Turn on File Sharing on Private networks
Set-NetFirewallRule -DisplayGroup "File And Printer Sharing" -Enabled True -Profile Private

# Turn on Microsoft Updates
(New-Object -ComObject Microsoft.Update.ServiceManager).AddService2("7971f918-a847-4430-9279-4a52d1efe18d", 7, "")

# Disable Windows Error Reporting
Disable-WindowsErrorReporting

# Configure Language and Keyboard Layouts
$LanguageList = New-WinUserLanguageList -Language "en-US"
$LanguageList[0].InputMethodTips.Clear()
$LanguageList[0].InputMethodTips.Add('0409:00020409')
$LanguageList[0].InputMethodTips.Add('0409:00000416')
Set-WinUserLanguageList -LanguageList $LanguageList -Force
