# Content to reinstall
$Reinstall_AppxPackage = New-Object -TypeName System.Collections.ArrayList
$Reinstall_AppxPackage.AddRange(@(
    "Microsoft.ZuneMusic",
    "Microsoft.ZuneVideo"
))

$Provisioned_AppxPackage = Get-AppxProvisionedPackage -Online | Select-Object -ExpandProperty DisplayName
foreach ($AppxPackage in $Provisioned_AppxPackage) {
    if ($AppxPackage -in $Reinstall_AppxPackage) {
        Get-AppxPackage -AllUsers -Name $AppxPackage | Foreach {Add-AppxPackage -DisableDevelopmentMode -Register "$($_.InstallLocation)\AppXManifest.xml"}
    }
}
